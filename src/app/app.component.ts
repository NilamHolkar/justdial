// app.component.ts
import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {


  currentPageTitle = 'Home';

  appPages = [
    {
      title: 'Home',
      url: '/first',
      icon: 'home'
    },
    {
      title: 'My Account',
      url: '/profile',
      icon: 'person'
    },
    {
      title: 'My Businesses',
      url: '/add-business',
      icon: 'briefcase'
    },
    {
      title: 'My Advertises',
      url: '/advertises',
      icon: 'image'
    },
    {
      title: 'Share',
      url: '/share',
      icon: 'share'
    },
    {
      title: 'Contact Us',
      url: '/contact',
      icon: 'call'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
    
  }

  backButtonSubscription;
  ngAfterViewInit() {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      // add logic here if you want to ask for a popup before exiting
      navigator['app'].exitApp();
    });
  }

  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
