import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditAdvertisesPageRoutingModule } from './edit-advertises-routing.module';

import { EditAdvertisesPage } from './edit-advertises.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditAdvertisesPageRoutingModule
  ],
  declarations: [EditAdvertisesPage]
})
export class EditAdvertisesPageModule {}
