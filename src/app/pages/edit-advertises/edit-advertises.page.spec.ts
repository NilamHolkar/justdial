import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditAdvertisesPage } from './edit-advertises.page';

describe('EditAdvertisesPage', () => {
  let component: EditAdvertisesPage;
  let fixture: ComponentFixture<EditAdvertisesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAdvertisesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditAdvertisesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
