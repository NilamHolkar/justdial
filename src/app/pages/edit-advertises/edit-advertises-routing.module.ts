import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditAdvertisesPage } from './edit-advertises.page';

const routes: Routes = [
  {
    path: '',
    component: EditAdvertisesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditAdvertisesPageRoutingModule {}
