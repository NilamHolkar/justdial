import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'first',
    loadChildren: () => import('./pages/first/first.module').then( m => m.FirstPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'add-business',
    loadChildren: () => import('./pages/add-business/add-business.module').then( m => m.AddBusinessPageModule)
  },
  {
    path: 'advertises',
    loadChildren: () => import('./pages/advertises/advertises.module').then( m => m.AdvertisesPageModule)
  },
  {
    path: 'second',
    loadChildren: () => import('./pages/second/second.module').then( m => m.SecondPageModule)
  },
  {
    path: 'third',
    loadChildren: () => import('./pages/third/third.module').then( m => m.ThirdPageModule)
  },
  {
    path: 'four',
    loadChildren: () => import('./pages/four/four.module').then( m => m.FourPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./pages/contact/contact.module').then( m => m.ContactPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'forgot',
    loadChildren: () => import('./forgot/forgot.module').then( m => m.ForgotPageModule)
  },
  {
    path: 'share',
    loadChildren: () => import('./pages/share/share.module').then( m => m.SharePageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./pages/edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'edit-business',
    loadChildren: () => import('./pages/edit-business/edit-business.module').then( m => m.EditBusinessPageModule)
  },
  {
    path: 'edit-advertises',
    loadChildren: () => import('./pages/edit-advertises/edit-advertises.module').then( m => m.EditAdvertisesPageModule)
  },
  {
    path: 'area',
    loadChildren: () => import('./pages/area/area.module').then( m => m.AreaPageModule)
  },
  {
    path: 'notify',
    loadChildren: () => import('./pages/notify/notify.module').then( m => m.NotifyPageModule)
  },
  
  {
    path: 'contents',
    loadChildren: () => import('./pages/contents/contents.module').then( m => m.ContentsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
